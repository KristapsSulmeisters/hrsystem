import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Department } from './department';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const departments = [
      { id: 1, title: 'IT' },
      { id: 2, title: 'FIN' },
      { id: 3, title: 'BOARD' },
      { id: 4, title: 'DOC' },
      { id: 5, title: 'RND' },
      { id: 6, title: 'RND' },
      { id: 7, title: 'HR' }


    ];
    return {departments};
  }


  genId(departments: Department[]): number {
    return departments.length > 0 ? Math.max(...departments.map(department => department.id)) + 1 : 11;
  }
}

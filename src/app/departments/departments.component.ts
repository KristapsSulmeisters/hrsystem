import { Component, OnInit } from '@angular/core';
import { Department} from "../Department";
import {DepartmentService} from "../department.service";

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
  departments: Department[]


  constructor( private departmentService: DepartmentService) { }

  ngOnInit(): void {
    this.getDepartments();
  }
  getDepartments(): void {
    this.departmentService.getDepartments()
      .subscribe(deparments => this.departments = deparments);
  }

  add(title: string): void {
    title = title.trim();
    if (!title) { return; }
    // @ts-ignore
    this.departmentService.addDeparment({ title } as Department)
      .subscribe(department => {
        this.departments.push(department);
      });
  }

  delete(department: Department): void {
    this.departments = this.departments.filter(d =>d !== department);
    this.departmentService.deleteHero(department).subscribe();
  }
  update(department: Department): void {
    this.departments = this.departments.filter(d =>d !== department);
    this.departmentService.deleteHero(department).subscribe();
  }
}

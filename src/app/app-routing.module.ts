import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";

import { DepartmentsComponent }   from './departments/departments.component';
import {PositionsComponent} from "./positions/positions.component";
import {EmployeesComponent} from "./employees/employees.component";

// import { HeroesComponent }      from './heroes/heroes.component';
// import { HeroDetailComponent }  from './hero-detail/hero-detail.component';

const routes: Routes = [
  // { path: '', redirectTo: '/departments', pathMatch: 'full' },
  { path: 'departments', component: DepartmentsComponent },
  { path: 'employees', component: EmployeesComponent },
  { path: 'positions', component: PositionsComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})



export class AppRoutingModule { }
